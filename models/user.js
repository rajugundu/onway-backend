const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    passwordHash: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        deafult: true,
        required: true
    },
    verified: {
        type: Boolean,
        default: false
    },
    doc: {
        type: String,
        deafult: ''
    },
    docId: {
        type: String,
        deafult: ''
    },
})

userSchema.virtual('id').get(function() {
    return this._id.toHexString()
})

userSchema.set('toJSON', {
    virtuals: true
})

exports.User = mongoose.model('User', userSchema)