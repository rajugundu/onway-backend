const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    senderAddressId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address'
    },
    receiverAddressId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address'
    },
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    status: {
        type: String,
        default: 'Pending'
    },
    total: {
        type: String,
        default: 0
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
})

orderSchema.virtual('id').get(function() {
    return this._id.toHexString()
})

orderSchema.set('toJSON', {
    virtuals: true
})

exports.Order = mongoose.model('Order', orderSchema)