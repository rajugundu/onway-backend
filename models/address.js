const mongoose = require('mongoose');

const addressSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    street: {
        type: String,
        required: true
    },
    apartment: {
        type: String,
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    zip: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    country: {
        type: String,
        default: 'India'
    }
})

addressSchema.virtual('id').get(function() {
    return this._id.toHexString()
})

addressSchema.set('toJSON', {
    virtuals: true
})

exports.Address = mongoose.model('Address', addressSchema)