const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    width: {
        type: Number,
        default: 0
    },
    height: {
        type: Number,
        default: 0
    },
    length: {
        type: Number,
        default: 0
    },
    weight: {
        type: Number,
        default: 0
    },
    image: {
        type: String,
        deafult: ''
    },
    images: [{
        type: String
    }],
    datePosted: {
        type: Date,
        deafult: Date.now,
    }
})

productSchema.virtual('id').get(function() {
    return this._id.toHexString()
})

productSchema.set('toJSON', {
    virtuals: true
})

exports.Product = mongoose.model('Product', productSchema)