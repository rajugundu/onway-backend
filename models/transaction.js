const mongoose = require('mongoose');

const transactionSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    order: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Order'
    },
    status: {
        type: String,
        default: 'Pending'
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
})

transactionSchema.virtual('id').get(function() {
    return this._id.toHexString()
})

transactionSchema.set('toJSON', {
    virtuals: true
})

exports.Transaction = mongoose.model('Transaction', transactionSchema)