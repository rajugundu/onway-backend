const express = require('express');
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose');
const authJwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');
const serverless = require('serverless-http');
const nodemailer = require("nodemailer");

require('dotenv/config')

const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const client = require('twilio')(accountSid, authToken);

// enabling cors
app.use(cors());
app.use(express.urlencoded({ extended: true, strict: false }));

// middleware
app.use(bodyParser.json())
app.use(morgan('tiny'))
app.use(authJwt())
app.use('/public/uploads', express.static(__dirname + '/public/uploads/'));
app.use(errorHandler)

// Routers
const productsRouter = require('./routers/products')
const addressRouter = require('./routers/addresses')
const userRouter = require('./routers/users')
const orderRouter = require('./routers/orders');
const transactionRouter = require('./routers/transactions');
const api = process.env.API_URL

// Route
app.use(`${api}/products`, productsRouter)
app.use(`${api}/users`, userRouter)
app.use(`${api}/orders`, orderRouter)
app.use(`${api}/addresses`, addressRouter)
app.use(`${api}/transactions`, transactionRouter)

app.post('/send-mail', async (req, res) => {

    const {name} = req.body
    const {mobile} = req.body
    const {type} = req.body
    const {text} = req.body

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
            user: 'onwaydelivers',
            pass: 'Onway@123'
        }       
    });

    const message = {
        from: '"On Way Delivers" <onwaydelivers@gmail.com',
        to: 'raju.damu25@gmail.com',
        subject: 'Website contact us form',
        html: 'Name: ' + name + "<br>" + 'Mobile: ' + mobile + "<br>" + 'Type: ' + type + "<br>" + 'Text: ' + text
    }

    const info = await transporter.sendMail(message)

    res.status(200).send({message: 'We have received your message. We will get back to you as soon as possible'})

})

app.post('/send-sms', async (req, res) => {
    const {code} = req.body
    const {mobile} = req.body
    client.messages
    .create({
        body: 'Thanks for signing up with us. Your verification code is: ' + code,
        from: '+14044766436',
        to: '+91'+mobile
    })
    .then(message => res.send(message.sid))
    .catch(err => console.log(err));

})

// Mongo DB connection
mongoose.connect(process.env.CONNECTTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: 'onway-database'
})
.then(() => {
    console.log('Mongo DB connected')
})
.catch ((err) => {
    console.log(err)
})

// Listen app to port
app.listen(3002, () => {
    console.log('App running on port: 3000')
})

module.exports.handler = serverless(app);