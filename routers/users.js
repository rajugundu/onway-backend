const express = require('express')
const router = express.Router();
const {User} = require('../models/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {check, validationResult} = require('express-validator/check');
const e = require('express');
const { uploadDocument } = require('./uploadDocument.js'); 

router.get(`/`, async (req, res) => {
    const token = req.headers['authorization'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.', token: token });
    
    const newtoken = token.replace("Bearer ","")

    jwt.verify(newtoken, process.env.SECRET, function(err, decoded) {
      if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.', token: newtoken});

        User.findById(decoded.userId, 
            { password: 0 },
            function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");
                
            res.status(200).send(user);
        }).select('-passwordHash -isAdmin');
    });
})

router.get(`/:id`, async (req, res) => {
    const user = await User.findById(req.params.id).select('-passwordHash -isAdmin')

    if(!user) {
        res.status(500).json({
            message: 'User with the given ID not found.'
        })
    }
    res.status(200).send(user)
})

function validateEmailAccessibility(email){

    return User.findOne({email: email}).then(function(result){
         return result !== null;
    });
 }

router.post(`/register`, async (req, res) => {

    const existUsername = await User.findOne({ phone: req.body.phone});
    const regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(req.body.name == '') {
        return res.status(400).send({message: 'Name cannot be empty', field: 'name-register'})
    }

    if(req.body.email == '' || regEmail.test(req.body.email) === false) {
        return res.status(400).send({message: 'Invalid email', field: 'email-register'})
    }

    if(req.body.password == '' || req.body.password.length < 8) {
        return res.status(400).send({message: 'Password must be of 8 characters', field: 'password-register'})
    }

    if(req.body.phone == '') {
        return res.status(400).send({message: 'Phone cannot be empty', field: 'phone-register'})
    } else if(existUsername) {
        return res.status(400).send({message: 'This phone is registered with other user', field: 'phone-register'})
    }

    validateEmailAccessibility(req.body.email).then(async function(valid) {
        if (valid) {
          return res.status(400).send({message: 'User with email is already registered', field: 'email-register'})
        } else {
            let user = new User({
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone,
                passwordHash: bcrypt.hashSync(req.body.password, 10),
                isAdmin: true,
            })
            user = await user.save()
        
            if(!user)
            return res.status(400).send('User cannot be created')
    
            // Logging user after registration
            const secret = process.env.SECRET
    
            if(user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
                const token = jwt.sign(
                    {
                        userId: user.id,
                        isAdmin: user.isAdmin
                    },
                    secret,
                    {
                        expiresIn: '7d'
                    }
                )
                return res.status(200).send({user: user, token: token})
            }
        }
    });

})

router.post('/login', async(req, res) => {
    const user = await User.findOne({email: req.body.email})

    if(req.body.email == '') {
        return res.status(400).send({message: 'Email cannot be empty', field: 'email'})
    }

    if(req.body.password == '') {
        return res.status(400).send({message: 'Password cannot be empty', field: 'password'})
    }
    
    const secret = process.env.SECRET
     if(!user) {
         return res.status(400).send({message: 'User not found', field: 'title'})
     }

     if(user && bcrypt.compareSync(req.body.password, user.passwordHash)) {
         const token = jwt.sign(
            {
                userId: user.id,
                isAdmin: user.isAdmin
            },
            secret,
            {
                expiresIn: '1d'
            }
         )
        return res.status(200).send({user: user, token: token})
     } else {
        return res.status(400).send({message: 'Invalid Password', field: 'title'})
     }
})

router.put(`/verification/:id`, uploadDocument.single('document'), async (req, res) => {

    const user = await User.findByIdAndUpdate(
        req.params.id, 
        {
            verified: false,
            doc: req.file.location,
            docId: req.body.docId
        },
        {new: true}
    )

    if(!user) {
        return res.status(404).send('User with this ID cannot be updated')
    }
    return res.status(200).send({message: 'We have received your document. You account will be verified with 10 minutes', field: 'success'})
})

module.exports = router;