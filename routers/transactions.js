const express = require('express')
const router = express.Router();
const {Transaction} = require('../models/transaction')

router.post(`/`, async (req, res) => {

    let transaction = new Transaction({
        order: req.body.orderId,
        user: req.body.userId,
    })
    transaction = await transaction.save()

    if(!transaction)
    return res.status(400).send('Transaction cannot be created')

    res.send(transaction)
})

router.get(`/:id`, async (req, res) => {

    const transaction = await Transaction.findById(req.params.id)
    .populate({path: 'order'})
    .populate({path: 'user'})

    if(!transaction) {
        res.status(500).json({success:false})
    }
    res.status(200).send(transaction)
})

router.get(`/user/:id`, async (req, res) => {

    const transactions = await Transaction.find ( { user: req.params.id } )
    .populate({path: 'order', populate: {path: 'productId senderAddressId receiverAddressId'}})
    
    if(!transactions) {
        res.status(500).json({success:false})
    }
    res.status(200).send(transactions)
})

module.exports = router;