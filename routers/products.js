const express = require('express')
const router = express.Router();
const {Product} = require('../models/product')
const { v4: uuidv4 } = require('uuid');
const multer = require("multer");
const AWS = require("aws-sdk");

router.get(`/`, async (req, res) => {
    const productList = await Product.find()
    // const productList = await Product.find(filter).select('name image')
    if(!productList) {
        res.status(500).json({success:false})
    }
    res.send(productList)
})

router.get(`/:id`, async (req, res) => {
    const product = await Product.findById(req.params.id)
    if(!product) {
        res.status(500).json({success:false})
    }
    res.send(product)
})

var storage = multer.memoryStorage();
var upload = multer({ 
    storage: storage,
    fileFilter(req, file, next) {
        var isPhoto =
          file.mimetype.startsWith("image/");
        if (isPhoto) {
          next(null, true);
        } else {
          next({ message: "That fileType isn't allowed!!" }, false);
        }
    }
});

router.post("/", upload.single("image"), async(req, res) => {
    
    const file = req.file;
    // const s3FileURL = 'arn:aws:s3:::onway-images';
    
    const date = Math.floor(Math.random()*1000000000);

    const extArray = file.mimetype.split("/");
    const extension = extArray[extArray.length - 1];

    const id = uuidv4()
  
    const s3bucket = new AWS.S3({
        accessKeyId: 'AKIASK2TO3RDSHDOGPN4',
        secretAccessKey: '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',
        region: 'us-east-2'
    });
    
    const params = {
        Bucket: 'onway-iamges',
        Key: id + date + '.' + extension,
        Body: file.buffer,
        ContentType: file.mimetype,
        ACL: "public-read",
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
    };
  
    await s3bucket.upload(params, async(err, data) => {
        if (err) {
            res.status(500).json({ error: true, Message: err });
        } else {
            // res.send({ data });
            // var newFileUploaded = {
            //     description: req.body.description,
            //     fileLink: s3FileURL + file.originalname,
            //     s3_key: params.Key
            // };
            // var document = new DOCUMENT(newFileUploaded);
            // document.save(function(error, newFile) {
            //     if (error) {
            //     throw error;
            //     }
            // });
            let product = await new Product({
                title: req.body.title,
                description: req.body.description,
                width: req.body.width,
                height: req.body.height, 
                length: req.body.length,
                weight: req.body.weight,
                image: data.Location
            })
        
            product = await product.save()
        
            if(!product)
            return res.status(500).send('Product cannot be created')
        
            return res.send(product)
        }
    });
});

module.exports = router;