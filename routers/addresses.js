const express = require('express')
const router = express.Router();
const {Address} = require('../models/address')

router.get(`/`, async (req, res) => {
    const addressList = await Address.find()

    if(!addressList) {
        res.status(500).json({success:false})
    }
    res.status(200).send(addressList)
})

router.post(`/`, async (req, res) => {

    let address = new Address({
        name: req.body.name,
        street: req.body.street,
        apartment: req.body.apartment,
        city: req.body.city,
        state: req.body.state,
        zip: req.body.zip,
        phone: req.body.phone,
    })
    address = await address.save()

    if(!address)
    return res.status(400).send('Address cannot be created')

    res.send(address)
})

router.get(`/:id`, async (req, res) => {
    const address = await Address.findById(req.params.id)

    if(!address) {
        res.status(500).json({success:false})
    }
    res.status(200).send(address)
})

module.exports = router;