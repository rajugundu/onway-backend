const express = require('express')
const router = express.Router();
const {Order} = require('../models/order')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

router.get(`/`, async (req, res) => {
    const orderList = await Order.find()
    .populate({path: 'productId'})
    .populate({path: 'userId'})
    .populate({path: 'senderAddressId'})
    .populate({path: 'receiverAddressId'})
    .sort({createdAt: -1})

    if(!orderList) {
        res.status(500).json({success:false})
    }
    res.status(200).send(orderList)
})

router.get(`/:id`, async (req, res) => {
    const order = await Order.findById(req.params.id)
    .populate({path: 'productId'})
    .populate({path: 'userId'})
    .populate({path: 'senderAddressId'})
    .populate({path: 'receiverAddressId'})


    if(!order) {
        res.status(500).json({success:false})
    }
    res.status(200).send(order)
})

router.post(`/`, async (req, res) => {

    let order = new Order({
        senderAddressId: req.body.senderAddressId,
        receiverAddressId: req.body.receiverAddressId,
        productId: req.body.productId,
        userId: req.body.userId,
    })
    order = await order.save()

    if(!order)
    return res.status(400).send('Order cannot be created')

    res.send(order)
})

router.put(`/:id`, async (req, res) => {
    if(!mongoose.isValidObjectId(req.params.id)) {
        res.status(400).send('Invalid order ID')
    }
    const order = await Order.findByIdAndUpdate(
        req.params.id, 
        {
            status: req.body.status,
        },
        {new: true}
    )

    if(!order)
    return res.status(404).send('Order with this ID cannot be updated')

    res.send(order)

})

router.get(`/user/:id`, async (req, res) => {
    const orders = await Order.find ({ userId: req.params.id })
    .populate('senderAddressId receiverAddressId productId')
    .sort({createdAt: -1})

    if(!orders) {
        res.status(500).json({success:false})
    }
    res.status(200).send(orders)
})

module.exports = router;