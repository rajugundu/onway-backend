const aws = require('aws-sdk')
const multer = require('multer')
const multerS3 = require('multer-s3')

aws.config.update({
    accessKeyId: 'AKIASK2TO3RDSHDOGPN4',
    secretAccessKey: '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',
    region: 'us-east-2'
})

const s3 = new aws.S3()

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true)
    } else {
        cb(new Error('Invalid Mime Type, only JPEG and PNG'), false);
    }
}

const upload = multer({
    fileFilter,
    storage: multerS3({
        s3: s3,
        bucket: 'onway-iamges',
        acl: "public-read",
        metadata: function(req, file, cb) {
            cb(null, {fieldName: 'TEST_META_DATA'})
        },
        key: function(req, file, cb) {
            cb(null, Date.now().toString())
        }
    })
})

module.exports = upload